﻿using Employers.Abstractions.Models;
using Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employers.Abstractions.Requests
{
    public class UpdateEmployerRequest : HaveFirstLastName
    {
        public string EmployerId { get; set; }

        public void UpdateEmployer(ref Employer employer)
        {
            employer.FirstName = FirstName ?? employer.FirstName;
            employer.LastName = LastName ?? employer.LastName;
        }
    }
}
