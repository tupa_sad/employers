﻿using Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Employers.Abstractions.Models
{
    public partial class Employer : HaveFirstLastName
    {
        public string EmployerId { get; set; }
    }
}
