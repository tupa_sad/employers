﻿using Employers.Abstractions.Models;
using Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Employers.Abstractions.Responses
{
    public class EmployerResponse : HaveFirstLastName
    {
        public EmployerResponse(Employer employer)
        {
            EmployerId = employer.EmployerId;
            FirstName = employer.FirstName;
            LastName = employer.LastName;
        }

        public string EmployerId { get; set; }
    }
}
