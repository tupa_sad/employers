﻿using Employers.Abstractions.Models;
using Employers.Abstractions.Requests;
using Employers.Abstractions.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Employers.Abstractions
{
    public interface IEmployerService
    {
        Task<IList<EmployerResponse>> GetAllAsResponses(int page, int perPage);
        Task<Employer> Get(string employerId);
        Task<Employer> Create(Employer employer);
        Task Delete(string employerId);
        Task<Employer> Update(UpdateEmployerRequest updateEmployerRequest);
    }
}
