﻿using Employers.Abstractions.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employers.Abstractions
{
    public interface IEmployerRepository
    {
        Task<IEnumerable<Employer>> GetAllEmployers(int page, int perPage);
        Task<Employer> GetEmployer(string employerId);
        Task<Employer> CreateEmployerAsync(Employer employer);
        Task DeleteEmployerAsync(Employer employer);
        Task UpdateEmployerAsync(Employer employer);
    }
}
