﻿using Employers.Abstractions.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employers.Api.Context
{
    public class EmployerContext : DbContext
    {
        public EmployerContext(DbContextOptions options) : base(options)
        {
        }

        public virtual DbSet<Employer> Employers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employer>(entity =>
            {
                entity.ToTable("employer");

                entity.Property(e => e.EmployerId)
                .HasColumnName("employer_id")
                .HasMaxLength(36);

                entity.Property(e => e.FirstName)
                .IsRequired()
                .HasColumnName("first_name")
                .HasMaxLength(255);

                entity.Property(e => e.LastName)
                .IsRequired()
                .HasColumnName("last_name")
                .HasMaxLength(255);
            });
        }
    }
}
