﻿using Employers.Abstractions;
using Employers.Api.Context;
using Employers.Api.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employers.Api.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection InitializeEmployersServices(
            this IServiceCollection services,
            Action<DbContextOptionsBuilder> options = null)
        {
            services.AddDbContext<EmployerContext>(options);
            services.TryAddScoped<IEmployerRepository, EmployerRepository>();
            services.TryAddScoped<IEmployerService, EmployerService>();
            return services;
        }
    }
}
