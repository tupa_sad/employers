﻿using Employers.Abstractions;
using Employers.Abstractions.Models;
using Employers.Abstractions.Requests;
using Employers.Abstractions.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Employers.Api.Services
{
    public class EmployerService : IEmployerService
    {
        private readonly EmployerRepository employerRepository;

        public EmployerService(EmployerRepository employerRepository)
        {
            this.employerRepository = employerRepository;
        }

        public async Task<Employer> Create(Employer сlient)
        {
            return await employerRepository.CreateEmployerAsync(сlient);
        }

        public async Task Delete(string сlientId)
        {
            Employer employer = await employerRepository.GetEmployer(сlientId);
            if (employer == null)
            {
                await employerRepository.DeleteEmployerAsync(employer);
            }
            return;
        }

        public async Task<Employer> Get(string сlientId)
        {
            Employer employer = await employerRepository.GetEmployer(сlientId);
            if (employer == null)
            {
                return null;
            }
            return employer;
        }

        public async Task<IList<EmployerResponse>> GetAllAsResponses(int page, int perPage)
        {
            var employersDb = await employerRepository.GetAllEmployers(page, perPage);

            var employers = new List<EmployerResponse>();
            foreach (var employerDb in employersDb)
            {
                employers.Add(new EmployerResponse(employerDb));
            }

            return employers;
        }

        public async Task<Employer> Update(UpdateEmployerRequest updateEmployerRequest)
        {
            Employer employer = await employerRepository.GetEmployer(updateEmployerRequest.EmployerId);
            updateEmployerRequest.UpdateEmployer(ref employer);
            await employerRepository.UpdateEmployerAsync(employer);
            return employer;
        }
    }
}
