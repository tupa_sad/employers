﻿using Employers.Abstractions;
using Employers.Abstractions.Models;
using Employers.Api.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employers.Api.Services
{
    public class EmployerRepository : IEmployerRepository
    {
        private readonly EmployerContext context;

        public async Task<IEnumerable<Employer>> GetAllEmployers(int page, int perPage)
        {
            IQueryable<Employer> request = context.Employers;
            if (page > 0 && perPage > 0)
            {
                request = request.Skip((page - 1) * perPage).Take(perPage);
            }

            request = request.OrderBy(x => x.LastName);
            return await request.ToListAsync();
        }

        public async Task<Employer> GetEmployer(string employerId)
        {
            Employer employer = context.Employers.FirstOrDefault(x => x.EmployerId == employerId);
            if (employer == null)
            {
                throw new Exception($"Employer with '{employerId}' id is not found. ({nameof(EmployerRepository)}.{nameof(GetEmployer)})");
            }
            return employer;
        }

        public async Task<Employer> CreateEmployerAsync(Employer employer)
        {
            context.Employers.Add(employer);
            await context.SaveChangesAsync();
            return employer;
        }

        public async Task DeleteEmployerAsync(Employer employer)
        {
            context.Remove(employer);
            await context.SaveChangesAsync();
        }

        public async Task UpdateEmployerAsync(Employer employer)
        {
            context.Update(employer);
            await context.SaveChangesAsync();
        }
    }
}
